const fs = require('fs-extra');
const concat = require('concat');

// get the directory name
const appConfig = require('./angular.json');

const elementOuputName = `custom-element`;

(async function build() {
  const files = [
    `./dist/${appConfig.defaultProject}/runtime.js`,
    `./dist/${appConfig.defaultProject}/polyfills.js`,
    `./dist/${appConfig.defaultProject}/scripts.js`,
    `./dist/${appConfig.defaultProject}/main.js`
  ];

  await fs.ensureDir('elements');
  await concat(files, `elements/${elementOuputName}.js`);
  console.log(
    `Custom element has been created: '/elements/${elementOuputName}.js'`
  );
})();
