import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { CustomComponent } from './custom/custom.component';

@NgModule({
  declarations: [CustomComponent],
  imports: [BrowserModule],
  providers: [],
  // Which component to load as the first one
  entryComponents: [CustomComponent]
})
export class AppModule {
  constructor(private injector: Injector) {
    const el = createCustomElement(CustomComponent, { injector });
    customElements.define('custom-element', el);
  }
  ngDoBootstrap() {}
}
